# Password Generator

A powerful JAVA application which allows the storage of any application credentials (username and password).

## Getting Started

Execute *PassGuard.exe* to use the application *(windows users for now)*. (todo)

### Prerequisites

Application tested with Java 16.0.2.  
* JavaFx (minimum version 8) must be installed to develop

### Installation

Important requisite are necessary to modify the source project :  
1. Every necessary libraries are already available in source code under ```/libraries```
2. JavaFx plugin must be enable in your IDE
3. JMetro path must be set in your IDE
4. JavaFx library path must be set in your IDE
5. **VM options** must be set to : ```--module-path "path/to/javaFx/lib" --add-modules javafx.controls``` *(runtime exception without)*

## Use

- A user account must be created in order to use the application
- Password generation is available for each new application registration
- Database is encrypted with AES256 to prevent any decryption
- Delete repository ```derby_storage``` to erase your account *(content will be permanently lost)*
- Possibility to update user account password **(not available)**
- *TIPS : right-click on your application row to copy on clipboard* ;)

## Interface

- **A secure login interface**

![Alt text](login.png?raw=true "Login")

- **Main interface of the application**

![Alt text](app.png?raw=true "Main Interface")

- **Record new application credentials**

![Alt text](record_new_credentials.png?raw=true "Application registration")

- **Update user account password**
  
![Alt text](update_user_account_password.png?raw=true "Update user account password")
## Running the tests

Tests can be executed here: ```src/application/test```

## Versioning

Version 2.0

## Authors

* [Overlorde](https://gitlab.com/Overlorde)

## License

This project is under MIT licence.

## Acknowledgments

*Created with Java & JavaFx love*