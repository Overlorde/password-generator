package utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/** 
* JDBCManager Tester. 
* 
* @author <Authors name> 
* @since <pre>mai 20, 2021</pre> 
* @version 1.0 
*/ 
public class JDBCManagerTest { 

@Before
public void before() {
} 

@After
public void after() {
} 

/** 
* 
* Method: initializeDatabase(Pair<String, String> identification) 
* 
*/ 
@Test
public void testInitializeDatabase() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: connectDatabase(Pair<String, String> identification) 
* 
*/ 
@Test
public void testConnectDatabase() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadApplicationsFromStorage(String password) 
* 
*/ 
@Test
public void testLoadApplicationsFromStorage() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadPasswordsFromStorage(String application, String password) 
* 
*/ 
@Test
public void testLoadPasswordsFromStorage() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteApplicationFromKeyStore(String applicationName, String password) 
* 
*/ 
@Test
public void testDeleteApplicationFromKeyStore() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: updateApplicationFromKeyStore(Pair<String, String> application, String password) 
* 
*/ 
@Test
public void testUpdateApplicationFromKeyStore() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: insertApplicationRecord(Pair<String, String> application, String password) 
* 
*/ 
@Test
public void testInsertApplicationRecord() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: updateAccountPassword(Pair<String, String> identification, String oldPassword) 
* 
*/ 
@Test
public void testUpdateAccountPassword() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: isDatabaseCreated() 
* 
*/ 
@Test
public void testIsDatabaseCreated() {
//TODO: Test goes here... 
} 


/** 
* 
* Method: deleteDatabaseDirectory() 
* 
*/ 
@Test
public void testDeleteDatabaseDirectory() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("deleteDatabaseDirectory"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: createUser(Pair<String, String> identification) 
* 
*/ 
@Test
public void testCreateUser() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("createUser", Pair<String,.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: createRole() 
* 
*/ 
@Test
public void testCreateRole() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("createRole"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: grantRole(String user) 
* 
*/ 
@Test
public void testGrantRole() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("grantRole", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: createTable(String user) 
* 
*/ 
@Test
public void testCreateTable() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("createTable", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: tryToConnectWithoutCredentials() 
* 
*/ 
@Test
public void testTryToConnectWithoutCredentials() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("tryToConnectWithoutCredentials"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: shutdownEngine(String password) 
* 
*/ 
@Test
public void testShutdownEngine() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("shutdownEngine", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: getConnectionURL(String alias, Pair<String, String> identification, boolean createDB, boolean shutdownDB) 
* 
*/ 
@Test
public void testGetConnectionURL() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("getConnectionURL", String.class, Pair<String,.class, boolean.class, boolean.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: createDatabaseKeyStore(String password) 
* 
*/ 
@Test
public void testCreateDatabaseKeyStore() {
//TODO: Test goes here... 
/* 
try { 
   Method method = JDBCManager.getClass().getMethod("createDatabaseKeyStore", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
