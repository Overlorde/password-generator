package utils;

import application.utils.Alias;
import application.utils.Encryption;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import static org.hamcrest.CoreMatchers.instanceOf;

/**
 * Encryption Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>mai 11, 2021</pre>
 */
public class EncryptionTest {
    private byte[] data;

    @Before
    public void before() throws NoSuchAlgorithmException {
        SecretKey key = KeyGenerator.getInstance("AES").generateKey();
        data = key.getEncoded();
    }

    @After
    public void after() {
    }

    /**
     * Method: getKeyStorePass()
     */
    @Test
    public void testGetKeyStorePass() {
        Assert.assertEquals(Alias.ACCOUNT.getAlias(), "keyStorePass.jks");
    }

    /**
     * Method: getKeyStoreDb()
     */
    @Test
    public void testGetKeyStoreDb() {
        Assert.assertEquals(Encryption.getKeyStoreDb(), "keyStoreDB.jks");
    }

    /**
     * Method: createKeyStore(final String keyStoreName, String password)
     */
    @Test
    public void testCreateKeyStore() {
        Assert.assertThat(Encryption.getOrCreateKeyStore(true, "hello world"), instanceOf(KeyStore.class));
    }

    /**
     * Method: encrypt(String sentence, byte[] keyParameter)
     */
    @Test
    public void testEncrypt() { //todo fix
        try {
            Method method = Encryption.class.getDeclaredMethod("encrypt", String.class, byte[].class);
            method.setAccessible(true);
            Assert.assertThat(method.invoke(Encryption.class, "hello", data), instanceOf(String.class));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
        }
    }

    /**
     * Method: decrypt(String sentence, byte[] keyParameter)
     */
    @Test
    public void testDecrypt() {
        try {
            Method encrypt = Encryption.class.getDeclaredMethod("encrypt", String.class, byte[].class);
            encrypt.setAccessible(true);
            String s = (String) encrypt.invoke(Encryption.class, "hello", data);
            Method method = Encryption.class.getDeclaredMethod("decrypt", String.class, byte[].class);
            method.setAccessible(true);
            Assert.assertEquals(method.invoke(Encryption.class, s, data), "hello");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
        }
    }

    /**
     * Method: getKeyFromStore(String alias, String password)
     */
    @Test
    public void testGetKeyFromStore() {
        Assert.assertNull(Encryption.getKeyFromStore("test", "hello world"));
    }


    /**
     * Method: getKey()
     */
    @Test
    public void testGetKey() {
        try {
            Method method = Encryption.class.getDeclaredMethod("getKey");
            method.setAccessible(true);
            Assert.assertThat(method.invoke(Encryption.class), instanceOf(SecretKey.class));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
        }

    }

} 
