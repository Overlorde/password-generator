package model;

import application.model.PasswordGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * PasswordGenerator Tester.
 *
 * @author Burnier
 * @version 1.0
 * @since <pre>mai 11, 2021</pre>
 */
public class PasswordGeneratorTest {

    @Before
    public void before() {
    }

    @After
    public void after() {
    }

    /**
     * Method: generatePassword(boolean choiceCharacter, int sizePassword)
     */
    @Test
    public void testGeneratePassword() {
        StringBuffer passwordGenerated = PasswordGenerator.generatePassword(true, 10);
        Assert.assertEquals(passwordGenerated.toString().length(), 10);
    }


} 
