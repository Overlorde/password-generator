package model;

import application.model.Account;
import javafx.util.Pair;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Account Tester.
 *
 * @author Burnier
 * @version 1.0
 * @since <pre>mai 11, 2021</pre>
 */
public class AccountTest {
    public Account account;

    @Before
    public void before() {
        account = new Account(new Pair<>("username", "password"));
    }

    @After
    public void after() {
    }

    /**
     * Method: getPassword()
     */
    @Test
    public void testGetPassword() {
        Assert.assertEquals(account.getPassword(), "password");
    }

    /**
     * Method: setPassword(String password)
     */
    @Test
    public void testSetPassword() {
        account.setPassword("password1");
        Assert.assertEquals(account.getPassword(), "password1");
    }

    /**
     * Method: getUsername()
     */
    @Test
    public void testGetUsername() {
        Assert.assertEquals(account.getUsername(), "username");
    }

    /**
     * Method: setUsername(String username)
     */
    @Test
    public void testSetUsername() {
        account.setUsername("username1");
        Assert.assertEquals(account.getUsername(), "username1");
    }
} 
