package view;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/** 
* Controller Tester. 
* 
* @author <Authors name> 
* @since <pre>mai 11, 2021</pre> 
* @version 1.0 
*/ 
public class ControllerTest { 

@Before
public void before() {
} 

@After
public void after() {
} 

/** 
* 
* Method: copyToClipBoard(String password) 
* 
*/ 
@Test
public void testCopyToClipBoard() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: setMainApp(MainApp mainApp) 
* 
*/ 
@Test
public void testSetMainApp() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: clickedOnCreateButton() 
* 
*/ 
@Test
public void testClickedOnCreateButton() {
//TODO: Test goes here... 
} 

/** 
* 
* Method: clickedOnSettingsButton() 
* 
*/ 
@Test
public void testClickedOnSettingsButton() {
//TODO: Test goes here... 
} 


/** 
* 
* Method: initialize() 
* 
*/ 
@Test
public void testInitialize() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("initialize"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: updateDialog(String application) 
* 
*/ 
@Test
public void testUpdateDialog() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("updateDialog", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: InitializeTable() 
* 
*/ 
@Test
public void testInitializeTable() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("InitializeTable"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: initializeGUI() 
* 
*/ 
@Test
public void testInitializeGUI() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("initializeGUI"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: calculatePasswordComplexityAndShow(String password) 
* 
*/ 
@Test
public void testCalculatePasswordComplexityAndShow() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("calculatePasswordComplexityAndShow", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: informWithDialog(AlertType alertType, String title, String header, String message) 
* 
*/ 
@Test
public void testInformWithDialog() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("informWithDialog", AlertType.class, String.class, String.class, String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: factoryDialog(int situation) 
* 
*/ 
@Test
public void testFactoryDialog() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("factoryDialog", int.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: initializeComponentsListener(int situation, PasswordField password, PasswordField password2, PasswordField password3, Label passwordTips, Label newPasswordTips, Label passwordSize, CheckBox checkBoxPasswordWithoutSpecialCharacters, CheckBox checkBoxPasswordWithSpecialCharacters, Spinner<Integer> spinner, TextField username, Node loginButton) 
* 
*/ 
@Test
public void testInitializeComponentsListener() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("initializeComponentsListener", int.class, PasswordField.class, PasswordField.class, PasswordField.class, Label.class, Label.class, Label.class, CheckBox.class, CheckBox.class, Spinner<Integer>.class, TextField.class, Node.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: refreshTable() 
* 
*/ 
@Test
public void testRefreshTable() {
//TODO: Test goes here... 
/* 
try { 
   Method method = Controller.getClass().getMethod("refreshTable"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
