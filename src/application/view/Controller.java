package application.view;

import application.MainApp;
import application.model.Account;
import application.model.PasswordGenerator;
import application.utils.JDBCManager;
import application.utils.ModalType;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.Pair;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.System.exit;

public class Controller {
    @FXML
    private Text vulnerabilityPasswordHint;
    @FXML
    private Button settings;
    @FXML
    private Button createKeyToStorageButton;
    @FXML
    private Text passwordHint;
    private static int old_index = -1;
    @FXML
    private TableColumn<Pair<String, Object>, String> deleteColumn;
    @FXML
    private TableView<Pair<String, String>> recordTableView;
    @FXML
    private TableColumn<Pair<String, Object>, String> applicationNameColumn;
    @FXML
    private TableColumn<Pair<String, Object>, String> passwordColumn;
    private static Account account;
    @FXML
    private TableColumn<Pair<String, Object>, String> editColumn;
    @FXML
    private Rectangle colorHint;
    private Dialog<Pair<String, String>> dialog;
    private boolean dialogCanceled = false;
    private MainApp mainApp;

    /**
     * The constructor. The constructor is called before the initialize() method.
     */
    public Controller() {
    }

    /**
     * Initializes the controller class. This method is automatically called after
     * the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // check if a database exist -> else wait for user registration -> create sysadmin with random password hidden in keystore -> use sysadmin to handle creation database and user -> connect with user -> if user change password -> decrypt sysadmin to all call system
        Optional<Pair<String, String>> identification;
        if (JDBCManager.isDatabaseCreated()) {
            // login dialog
            identification = Optional.ofNullable(factoryDialog(ModalType.LOGIN));
        } else {
            // sign up dialog
            identification = Optional.ofNullable(factoryDialog(ModalType.SIGNUP));
            // initialize database components
            identification.ifPresent(JDBCManager::initializeDatabase);
        }
        if (identification.isPresent()) {
            try {
                // connect to database with user credentials
                boolean is_logged = JDBCManager.connectDatabase(identification.get());
                while (!is_logged) {
                    identification = this.dialog.showAndWait();
                    if (identification.isPresent()) {
                        is_logged = JDBCManager.connectDatabase(identification.get());
                    } else {
                        throw new Exception("An error occurred during login session.");
                    }
                }
                account = new Account(identification.get());
                initializeGUI();
                refreshTable();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Update the application password
     *
     * @param application The application name
     */
    private void updateDialog(String application) {
        Pair<String, String> identification = factoryDialog(ModalType.UPDATE);
        if (identification != null && !identification.getValue().isEmpty()) {
            try {
                if (!JDBCManager.updateApplicationFromKeyStore(new Pair<>(application, identification.getValue()), account.getPassword())) {
                    showInfoDialog(AlertType.INFORMATION, "Information", "Illegal argument", "Password is not the same in both.");
                }
                refreshTable();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initialize Table components
     */
    private void InitializeTable() {
        applicationNameColumn.setCellValueFactory(pairStringCellDataFeatures -> new SimpleStringProperty(String.valueOf(pairStringCellDataFeatures.getValue().getKey())));
        passwordColumn.setCellValueFactory(pairStringCellDataFeatures -> new SimpleStringProperty(String.valueOf(pairStringCellDataFeatures.getValue().getValue())));
        applicationNameColumn.setCellFactory(tc -> {
            TableCell<Pair<String, Object>, String> cell = new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setStyle("-fx-font-size:" + 16 + ";");
                    setStyle("-fx-alignment: CENTER;");
                    setText(item);
                }
            };

            cell.setOnMouseClicked(event -> {
                if (!cell.isEmpty()) {
                    try {
                        JDBCManager.loadPasswordsFromStorage(applicationNameColumn.getCellData(cell.getIndex()), account.getPassword());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
            return cell;
        });
        passwordColumn.setCellFactory(tc -> {
            TableCell<Pair<String, Object>, String> cell = new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setStyle("-fx-font-size:" + 16 + ";");
                    setStyle("-fx-alignment: CENTER;");
                    setText(item);
                }
            };

            cell.setOnMouseClicked(event -> {
                if (!cell.isEmpty()) {
                    try {
                        if (event.getButton().equals(MouseButton.PRIMARY)) {
                            if (old_index == -1) {
                                old_index = cell.getIndex();
                            }
                            if (old_index != cell.getIndex()) {
                                recordTableView.getItems().set(old_index, new Pair<>(applicationNameColumn.getCellData(old_index), IntStream.range(0, passwordColumn.getCellData(old_index).length()).mapToObj(i -> "\u2022").collect(Collectors.joining(""))));
                            }
                            if (recordTableView.getItems().get(cell.getIndex()).getValue().contains("\u2022")) {
                                String password = JDBCManager.loadPasswordsFromStorage(applicationNameColumn.getCellData(cell.getIndex()), account.getPassword());
                                recordTableView.getItems().set(cell.getIndex(), new Pair<>(applicationNameColumn.getCellData(cell.getIndex()), password));
                                calculatePasswordComplexityAndShow(password);
                                Platform.runLater(() -> {
                                    recordTableView.requestFocus();
                                    recordTableView.getSelectionModel().select(cell.getIndex());
                                    recordTableView.getFocusModel().focus(cell.getIndex());
                                });
                            } else {
                                recordTableView.getItems().set(cell.getIndex(), new Pair<>(applicationNameColumn.getCellData(cell.getIndex()), IntStream.range(0, passwordColumn.getCellData(cell.getIndex()).length()).mapToObj(i -> "\u2022").collect(Collectors.joining(""))));
                                vulnerabilityPasswordHint.setVisible(false);
                                colorHint.setFill(Paint.valueOf("#e9e2e2"));
                            }
                            old_index = cell.getIndex();
                        } else if (event.getButton().equals(MouseButton.SECONDARY)) {
                            copyToClipBoard(recordTableView.getItems().get(cell.getIndex()).getValue());
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
            return cell;
        });
        editColumn.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Pair<String, Object>, String> call(TableColumn<Pair<String, Object>, String> pairStringTableColumn) {
                return new TableCell<>() {
                    private final Button button = new Button();

                    {
                        button.setOnAction((ActionEvent event) -> updateDialog(getTableView().getItems().get(getIndex()).getKey()));
                    }

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            double radius = 15;
                            Circle circle = new Circle(radius);
                            ImageView buttonImage = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("resources/edit.png"))));
                            buttonImage.setFitWidth(26);
                            buttonImage.setPreserveRatio(true);
                            buttonImage.setSmooth(true);
                            buttonImage.setCache(true);
                            button.setShape(circle);
                            button.setMinSize(2 * radius, 2 * radius);
                            button.setMaxSize(2 * radius, 2 * radius);
                            button.setGraphic(buttonImage);
                            button.setStyle("-fx-alignment: CENTER;");
                            setGraphic(button);
                            setStyle("-fx-alignment: CENTER;");
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        }
                        setText(item);
                    }
                };
            }
        });
        deleteColumn.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Pair<String, Object>, String> call(TableColumn<Pair<String, Object>, String> pairStringTableColumn) {
                return new TableCell<>() {
                    private final Button button = new Button();

                    {
                        button.setOnAction((ActionEvent event) -> {
                            if (JDBCManager.deleteApplicationFromKeyStore(getTableView().getItems().get(getIndex()).getKey(), account.getPassword())) {
                                Optional<ButtonType> result = showInfoDialog(AlertType.CONFIRMATION, "Confirmation", " Are you sure you want to delete ?", "Application credentials will be permanently erased.");
                                if (result.get() == ButtonType.OK) {
                                    getTableView().getItems().remove(getIndex());
                                }
                            } else {
                                // delete operation has failed with KeyStore
                                showInfoDialog(AlertType.ERROR, "Error", "Error", "Something went wrong...");
                            }
                        });
                    }

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            ImageView buttonImage = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("resources/delete.png"))));
                            double radius = 15;
                            Circle circle = new Circle(radius);
                            buttonImage.setFitWidth(26);
                            buttonImage.setFitHeight(26);
                            button.setShape(circle);
                            button.setMinSize(2 * radius, 2 * radius);
                            button.setMaxSize(2 * radius, 2 * radius);
                            button.setGraphic(buttonImage);
                            button.setStyle("-fx-alignment: CENTER;");
                            setStyle("-fx-alignment: CENTER;");
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            setGraphic(button);
                        }
                        setText(item);
                    }
                };
            }
        });

    }

    /**
     * Initialize the GUI
     */
    private void initializeGUI() {
        vulnerabilityPasswordHint.setVisible(false);
        passwordHint.setFont(Font.font("System", FontWeight.BOLD, 12));
        createKeyToStorageButton.setStyle("-fx-background-radius: 25; -fx-border-radius: 25; -fx-background-color: #24A0ED; -fx-font-size:16;-fx-font-weight: bold;");
        double radius = 15;
        Circle circle = new Circle(radius);
        ImageView buttonImage = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("resources/settings.png"))));
        buttonImage.setFitWidth(20);
        buttonImage.setFitHeight(20);
        settings.setShape(circle);
        settings.setMinSize(2 * radius, 2 * radius);
        settings.setMaxSize(2 * radius, 2 * radius);
        settings.setGraphic(buttonImage);
        settings.setStyle("-fx-alignment: CENTER;");
        settings.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        settings.setStyle("-fx-background-radius: 100;");
        settings.setDisable(true); // todo fix updateAccountPassword
        InitializeTable();
        recordTableView.setEditable(false);
    }

    /**
     * Calculate the password complexity and display it to the user
     *
     * @param password The input password
     */
    private void calculatePasswordComplexityAndShow(String password) {
        int nScore = 0;
        vulnerabilityPasswordHint.setFont(Font.font("System", FontWeight.BOLD, 12));
        vulnerabilityPasswordHint.setVisible(true);
        // Password length
        // -- Less than 4 characters
        if (password.length() < 5) {
            nScore += 5;
        }
        // -- 5 to 7 characters
        else if (password.length() < 8) {
            nScore += 10;
        }
        // -- 8 or more
        else {
            nScore += 25 + (password.length() - 8);
        }

        // Letters
        int upperCount = 0;
        int lowerCount = 0;
        int numberCount = 0;
        int characterCount = 0;

        for (int i = 0; i < password.length(); i++) {
            if (Character.isUpperCase(password.charAt(i))) {
                upperCount += 1;
            } else if (Character.isLowerCase(password.charAt(i))) {
                lowerCount += 1;
            } else if (Character.isDigit(password.charAt(i))) {
                numberCount += 1;
            }
            // special character if not other category
            else {
                characterCount += 1;
            }
        }

        int nLowerUpperCount = upperCount + lowerCount;
        // -- Letters are all lower case
        if (upperCount == 0 && lowerCount != 0) {
            nScore += 10;
        }
        // -- Letters are upper case and lower case
        else if (upperCount != 0 && lowerCount != 0) {
            nScore += 20;
        }

        // -- 1 number
        if (numberCount == 1) {
            nScore += 10;
        }
        // -- 3 or more numbers
        if (numberCount >= 3) {
            nScore += 20;
        }

        // -- 1 character
        if (characterCount == 1) {
            nScore += 10;
        }
        // -- More than 1 character
        if (characterCount > 1) {
            nScore += 25;
        }

        // Bonus
        // -- Letters and numbers
        if (numberCount != 0 && nLowerUpperCount != 0) {
            nScore += 2;
        }
        // -- Letters, numbers, and characters
        if (numberCount != 0 && nLowerUpperCount != 0 && characterCount != 0) {
            nScore += 3;
        }
        // -- Mixed case letters, numbers, and characters
        if (numberCount != 0 && upperCount != 0 && lowerCount != 0 && characterCount != 0) {
            nScore += 5;
        }


        if (nScore >= 80) {
            vulnerabilityPasswordHint.setText("Very Strong");
            vulnerabilityPasswordHint.setFill(Paint.valueOf("#008000"));
            colorHint.setFill(Paint.valueOf("#008000"));
        }
        // -- Strong
        else if (nScore >= 60) {
            vulnerabilityPasswordHint.setText("Strong");
            vulnerabilityPasswordHint.setFill(Paint.valueOf("#006000"));
            colorHint.setFill(Paint.valueOf("#006000"));
        }
        // -- Average
        else if (nScore >= 40) {
            vulnerabilityPasswordHint.setText("Average");
            vulnerabilityPasswordHint.setFill(Paint.valueOf("#e3cb00"));
            colorHint.setFill(Paint.valueOf("#e3cb00"));
        }
        // -- Weak
        else if (nScore >= 20) {
            vulnerabilityPasswordHint.setText("Weak");
            vulnerabilityPasswordHint.setFill(Paint.valueOf("#Fe3d1a"));
            colorHint.setFill(Paint.valueOf("#Fe3d1a"));
        }
        // -- Very Weak
        else {
            vulnerabilityPasswordHint.setText("Very Weak");
            vulnerabilityPasswordHint.setFill(Paint.valueOf("#e71a1a"));
            colorHint.setFill(Paint.valueOf("#e71a1a"));
        }
    }

    /**
     * Copy the selected password to the clipboard
     *
     * @param password The password to copy to clipboard
     */
    public void copyToClipBoard(String password) {
        showInfoDialog(AlertType.INFORMATION, "Information", "Clipboard", "Password has been copied to clipboard !");
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(password);
        clipboard.setContent(content);
    }

    /**
     * Set the application
     *
     * @param mainApp the application
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Create the dialog
     *
     * @param alertType The alertType of the modal
     * @param title     The title of the modal
     * @param header    The header of the modal
     * @param message   The message of the modal
     */
    private Optional<ButtonType> showInfoDialog(AlertType alertType, String title, String header, String message) {
        Alert alert = new Alert(alertType);
        ImageView imageView;
        if (alertType.equals(AlertType.CONFIRMATION)) {
            imageView = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/question.png")).toString());
        } else {
            imageView = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/information_logo.png")).toString());
        }
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle(title);
        imageView.setFitHeight(50);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);
        alert.setGraphic(imageView);
        alert.setHeaderText(header);
        alert.setContentText(message);
        return alert.showAndWait();
    }

    /**
     * Handle the dialog form
     *
     * @param modalType the form number corresponding to a view situation
     * @return The credentials
     */
    private Pair<String, String> factoryDialog(ModalType modalType) {
        //Create the custom dialog.
        this.dialog = new Dialog<>();
        if (mainApp != null) {
            this.dialog.initOwner(mainApp.getPrimaryStage());
        }
        GridPane grid = new GridPane();
        // Create the username and password labels and fields.
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        Map<String, PasswordField> passwordFieldMap = new HashMap<>();
        passwordFieldMap.put("password", new PasswordField());
        passwordFieldMap.put("passwordConfirmation1", new PasswordField());
        passwordFieldMap.put("passwordConfirmation2", new PasswordField());
        // Set the minimum size of the password field to avoid resize by label
        passwordFieldMap.get("password").setMinWidth(300);
        passwordFieldMap.get("password").setPromptText("Password");
        passwordFieldMap.get("passwordConfirmation1").setMinWidth(300);
        passwordFieldMap.get("passwordConfirmation2").setMinWidth(300);

        Map<String, Label> labelMap = new HashMap<>();
        labelMap.put("passwordTips", new Label("Password does not correspond to the actual one."));
        labelMap.put("newPasswordTips", new Label("Password length must have at least a size of 8."));
        labelMap.put("passwordSize", new Label("Password size generated:"));
        labelMap.get("newPasswordTips").setVisible(false);
        labelMap.get("passwordTips").setVisible(false);

        Map<String, String> stringMap = new HashMap<>();
        stringMap.put("loginMessage", null);
        stringMap.put("loginButtonMessage", null);
        stringMap.put("headerMessage", null);
        CheckBox checkBoxPasswordWithoutSpecialCharacters = new CheckBox("Generate a password without special characters");
        CheckBox checkBoxPasswordWithSpecialCharacters = new CheckBox("Generate a password with special characters");

        Spinner<Integer> spinner = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(8, 24, 20));
        spinner.setDisable(true);

        TextField username = new TextField();

        ImageView loginImage;

        grid.add(passwordFieldMap.get("password"), 1, 1);
        grid.add(new Label("Password:"), 0, 1);

        if (modalType.equals(ModalType.LOGIN)) { //Login dialog
            stringMap.put("loginMessage", "Login");
            stringMap.put("loginButtonMessage", "Login");
            stringMap.put("headerMessage", "Please sign in to allow the password decryption");
            loginImage = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/login.png")).toString());
            username.setPromptText("Username");
            grid.add(new Label("Username:"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(labelMap.get("passwordTips"), 1, 2);
        } else if (modalType.equals(ModalType.REGISTER)) { //Register application dialog
            stringMap.put("loginMessage", "Add Entry");
            stringMap.put("headerMessage", "Enter your new application");
            stringMap.put("loginButtonMessage", "Save");
            loginImage = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/create.png")).toString());
            username.setPromptText("ex : Google");
            grid.add(new Label("Application name:"), 0, 0);
            grid.add(checkBoxPasswordWithoutSpecialCharacters, 1, 2);
            grid.add(checkBoxPasswordWithSpecialCharacters, 1, 3);
            grid.add(labelMap.get("passwordSize"), 0, 4);
            grid.add(spinner, 1, 4);
            grid.add(username, 1, 0);
            spinner.setVisible(false);
            labelMap.get("passwordSize").setVisible(false);
        } else if (modalType.equals(ModalType.UPDATE)) { //Update application password dialog
            stringMap.put("loginMessage", "Update Entry");
            stringMap.put("headerMessage", "Enter a new password for the application");
            stringMap.put("loginButtonMessage", "Save");
            loginImage = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/update_dialog.png")).toString());
            username.setVisible(false);
        } else if (modalType.equals(ModalType.SIGNUP)) { //Register account dialog
            stringMap.put("loginMessage", "Sign up");
            stringMap.put("headerMessage", "Please sign up your account to secure your passwords");
            stringMap.put("loginButtonMessage", "Sign up");
            loginImage = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/register_logo.jpg")).toString());
            username.setPromptText("Username");
            passwordFieldMap.get("passwordConfirmation1").setPromptText("Password");
            grid.add(new Label("Username:"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(new Label("Password:"), 0, 2);
            grid.add(passwordFieldMap.get("passwordConfirmation1"), 1, 2);
            grid.add(labelMap.get("passwordTips"), 1, 3);
        } else if (modalType.equals(ModalType.ACCOUNT)) { //Update password dialog
            stringMap.put("loginMessage", "Update Account");
            stringMap.put("headerMessage", "Change your password account");
            stringMap.put("loginButtonMessage", "Update");
            loginImage = new ImageView(Objects.requireNonNull(this.getClass().getResource("resources/settings.png")).toString());
            passwordFieldMap.get("passwordConfirmation1").setPromptText("New Password");
            passwordFieldMap.get("passwordConfirmation2").setPromptText("New Password");
            grid.add(new Label("New Password:"), 0, 3);
            grid.add(new Label("New Password:"), 0, 4);
            grid.add(labelMap.get("passwordTips"), 1, 2);
            grid.add(passwordFieldMap.get("passwordConfirmation1"), 1, 3);
            grid.add(passwordFieldMap.get("passwordConfirmation2"), 1, 4);
            grid.add(labelMap.get("newPasswordTips"), 1, 5);
        } else {
            throw new IllegalArgumentException("An exception occurred during dialog display.");
        }

        dialog.setTitle(stringMap.get("loginMessage"));
        dialog.setHeaderText(stringMap.get("headerMessage"));
        dialog.setGraphic(loginImage);


        // Set the icon (must be included in the project).
        loginImage.setFitHeight(70.0);
        loginImage.setFitWidth(70.0);
        // Set the button types.
        ButtonType loginButtonType = new ButtonType(stringMap.get("loginButtonMessage"), ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        ChangeListener<String> textListener = initializeComponentsListener(modalType, passwordFieldMap, labelMap,
                checkBoxPasswordWithoutSpecialCharacters, checkBoxPasswordWithSpecialCharacters, spinner, username, loginButton);

        username.textProperty().addListener(textListener);
        passwordFieldMap.get("password").textProperty().addListener(textListener);
        passwordFieldMap.get("passwordConfirmation2").textProperty().addListener(textListener);
        passwordFieldMap.get("passwordConfirmation1").textProperty().addListener(textListener);
        dialog.getDialogPane().setContent(grid);
        // Request focus on the username field by default.
        Platform.runLater(username::requestFocus);
        this.dialog.showAndWait(); // block user until right credentials
        if (modalType.equals(ModalType.ACCOUNT)) {
            passwordFieldMap.get("password").setText(passwordFieldMap.get("passwordConfirmation1").getText());
        }
        if (dialogCanceled) {
            dialogCanceled = false;
            return null;
        }
        this.dialog.setResultConverter(dialogButton -> {
            if (dialogButton.equals(loginButtonType)) {
                return new Pair<>(username.getText(), passwordFieldMap.get("password").getText());
            }
            return null;
        });
        return new Pair<>(username.getText(), passwordFieldMap.get("password").getText());
    }

    /**
     * Listens to any changes in password fields and checkboxes
     *
     * @param modalType                                The modal type as ModalType
     * @param passwordFieldHashMap                     The Map containing the password fields
     * @param labelHashMap                             The Map containing the label fields
     * @param checkBoxPasswordWithoutSpecialCharacters The checkbox to generate a password without special character
     * @param checkBoxPasswordWithSpecialCharacters    The checkbox to generate a password with special character
     * @param spinner                                  The spinner to select the length of password
     * @param username                                 The username of the user
     * @param loginButton                              The button to confirm the update
     * @return ChangeListener<String>
     */
    private ChangeListener<String> initializeComponentsListener(ModalType modalType, Map<String, PasswordField> passwordFieldHashMap, Map<String, Label> labelHashMap,
                                                                CheckBox checkBoxPasswordWithoutSpecialCharacters, CheckBox checkBoxPasswordWithSpecialCharacters,
                                                                Spinner<Integer> spinner, TextField username, Node loginButton) {
        ChangeListener<Boolean> checkBoxListener = ((observable, oldValue, newValue) -> {
            passwordFieldHashMap.get("password").clear();
            spinner.setDisable(!checkBoxPasswordWithoutSpecialCharacters.isSelected() && !checkBoxPasswordWithSpecialCharacters.isSelected());
            spinner.setVisible(checkBoxPasswordWithoutSpecialCharacters.isSelected() || checkBoxPasswordWithSpecialCharacters.isSelected());
            passwordFieldHashMap.get("password").setText(String.valueOf(PasswordGenerator.generatePassword(checkBoxPasswordWithSpecialCharacters.isSelected(), spinner.getValue())));
            checkBoxPasswordWithoutSpecialCharacters.setDisable(checkBoxPasswordWithSpecialCharacters.isSelected());
            checkBoxPasswordWithSpecialCharacters.setDisable(checkBoxPasswordWithoutSpecialCharacters.isSelected());
            if (!checkBoxPasswordWithSpecialCharacters.isSelected() && !checkBoxPasswordWithoutSpecialCharacters.isSelected()) {
                passwordFieldHashMap.get("password").clear();
            }
        });
        final Button cancel = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancel.addEventFilter(ActionEvent.ACTION, event -> {
            if (modalType.equals(ModalType.LOGIN) || modalType.equals(ModalType.SIGNUP)) {
                exit(0);
            } else {
                dialog.close();
                dialogCanceled = true;
            }
        });

        checkBoxPasswordWithoutSpecialCharacters.selectedProperty().addListener(checkBoxListener);
        checkBoxPasswordWithSpecialCharacters.selectedProperty().addListener(checkBoxListener);

        ChangeListener<String> textListener = ((observable, oldValue, newValue) -> {
            if ((modalType.equals(ModalType.LOGIN))) { //Login dialog -> disable ok button conditions
                if (username.getText().length() < 8 && !username.getText().isEmpty()) {
                    loginButton.setDisable(true);
                    labelHashMap.get("passwordTips").setVisible(true);
                    labelHashMap.get("passwordTips").setText("Username length must be at least of size 8.");
                } else if ((passwordFieldHashMap.get("password").getText().length() < 8 && !passwordFieldHashMap.get("password").getText().isEmpty())) { // Sign up account dialog ->
                    labelHashMap.get("passwordTips").setVisible(true);
                    labelHashMap.get("passwordTips").setText("Password length must be at least of size 8."); // Tips: Increase security with a mix of number, upper, lower and special characters.
                } else if(!(passwordFieldHashMap.get("password").getText().length() < 8) && !(username.getText().length() < 8)) {
                    loginButton.setDisable(false);
                    labelHashMap.get("passwordTips").setVisible(false);
                }else {
                    loginButton.setDisable(true);
                }
            } else if (modalType.equals(ModalType.SIGNUP)) { //Sign up account dialog -> enable ok button conditions
                if (username.getText().length() > 7 && passwordFieldHashMap.get("password").getText().length() > 7 && passwordFieldHashMap.get("password").getText().equals(passwordFieldHashMap.get("passwordConfirmation1").getText())) {
                    loginButton.setDisable(false);
                    labelHashMap.get("passwordTips").setVisible(false);
                } else if ((passwordFieldHashMap.get("password").getText().length() < 8 && !passwordFieldHashMap.get("password").getText().isEmpty()) ||
                        (passwordFieldHashMap.get("passwordConfirmation1").getText().length() < 8) && !passwordFieldHashMap.get("passwordConfirmation1").getText().isEmpty()) { // Sign up account dialog ->
                    loginButton.setDisable(true);
                    labelHashMap.get("passwordTips").setVisible(true);
                    labelHashMap.get("passwordTips").setText("Password length must be at least of size 8."); // Tips: Increase security with a mix of number, upper, lower and special characters.
                } else if (username.getText().length() < 8 && !username.getText().isEmpty()) {
                    loginButton.setDisable(true);
                    labelHashMap.get("passwordTips").setVisible(true);
                    labelHashMap.get("passwordTips").setText("Username length must be at least of size 8.");
                } else {
                    loginButton.setDisable(true);
                    labelHashMap.get("passwordTips").setVisible(false);
                }
                if (!passwordFieldHashMap.get("passwordConfirmation1").getText().equals(passwordFieldHashMap.get("password").getText()) && !passwordFieldHashMap.get("password").getText().isEmpty()) {
                    loginButton.setDisable(true);
                    labelHashMap.get("passwordTips").setVisible(true);
                    labelHashMap.get("passwordTips").setText("Passwords are not equal.");
                }
            } else if (modalType.equals(ModalType.REGISTER) || modalType.equals(ModalType.UPDATE)) {
                loginButton.setDisable(passwordFieldHashMap.get("password").getText().isEmpty() || (username.isVisible() && username.getText().isEmpty()));
            } else if (modalType.equals(ModalType.ACCOUNT)) {
                // password correspond to user password and new password1 and password2 have size superior to 7 and are equals
                if (passwordFieldHashMap.get("passwordConfirmation1").getText().length() > 7 && passwordFieldHashMap.get("password").getText().equals(account.getPassword()) &&
                        passwordFieldHashMap.get("passwordConfirmation1").getText().equals(passwordFieldHashMap.get("passwordConfirmation2").getText())) {
                    labelHashMap.get("passwordTips").setVisible(false);
                    labelHashMap.get("newPasswordTips").setVisible(false);
                    loginButton.setDisable(false);
                }
                // password1 or password2 length is inferior to 8
                if ((passwordFieldHashMap.get("passwordConfirmation1").getText().length() < 8 || passwordFieldHashMap.get("passwordConfirmation2").getText().length() < 8) &&
                        (!passwordFieldHashMap.get("passwordConfirmation1").getText().isEmpty() || !passwordFieldHashMap.get("passwordConfirmation2").getText().isEmpty())) {
                    labelHashMap.get("newPasswordTips").setVisible(true);
                    labelHashMap.get("newPasswordTips").setText("Password length must have at least a size of 8.");
                    loginButton.setDisable(true);
                } else if (!passwordFieldHashMap.get("passwordConfirmation1").getText().equals(passwordFieldHashMap.get("passwordConfirmation2").getText())) {
                    labelHashMap.get("newPasswordTips").setVisible(true);
                    labelHashMap.get("newPasswordTips").setText("Both password are not equal.");
                    loginButton.setDisable(true);
                }
                // password is not equal to user password
                labelHashMap.get("passwordTips").setVisible(!passwordFieldHashMap.get("password").getText().equals(account.getPassword()) && !passwordFieldHashMap.get("password").getText().isEmpty());
                labelHashMap.get("passwordTips").setText("Entered password does not correspond to the actual password.");
                loginButton.setDisable(!passwordFieldHashMap.get("password").getText().equals(account.getPassword()) && !passwordFieldHashMap.get("password").getText().isEmpty());
                // both new password are not equals


            } else {
                throw new IllegalArgumentException("Dialog case not implemented !");
            }
        });

        spinner.valueProperty().addListener((obs, oldValue, newValue) -> passwordFieldHashMap.get("password").setText(String.valueOf(PasswordGenerator.generatePassword(true, spinner.getValue()))));

        return textListener;
    }

    /**
     * Handle the click on the create button to register a new application credentials
     *
     * @throws Exception an Exception
     */
    public void clickedOnCreateButton() throws Exception {
        Pair<String, String> applicationNameAndPassword = factoryDialog(ModalType.REGISTER);
        if (applicationNameAndPassword != null && !applicationNameAndPassword.getKey().isEmpty() && !applicationNameAndPassword.getValue().isEmpty()) {
            if (!JDBCManager.insertApplicationRecord(applicationNameAndPassword, account.getPassword())) {
                showInfoDialog(AlertType.INFORMATION, "Information", "Illegal argument", "Application name is already defined in Key Store.");
            }
            refreshTable();
        }
    }

    /**
     * Refresh the table
     *
     * @throws Exception an Exception
     */
    private void refreshTable() throws Exception {
        if (!account.getPassword().isEmpty()) {
            ArrayList<Pair<String, Integer>> applications = JDBCManager.loadApplicationsFromStorage(account.getPassword());
            ArrayList<Pair<String, String>> pairs = new ArrayList<>();
            for (Pair<String, Integer> app : applications) {
                pairs.add(new Pair<>(app.getKey(), IntStream.range(0, app.getValue()).mapToObj(i -> "\u2022").collect(Collectors.joining(""))));
            }
            applications.clear();
            recordTableView.setItems(FXCollections.observableArrayList(pairs)); //see table columns
            recordTableView.refresh();
        } else {
            throw new IllegalArgumentException("User's password is not defined. Exit.");
        }
    }

    /**
     * Handle the click on settings button to update the account password
     */
    public void clickedOnSettingsButton() {
        Pair<String, String> identification = factoryDialog(ModalType.ACCOUNT);
        if (identification != null && !identification.getValue().isEmpty()) {
            if (JDBCManager.updateAccountPassword(new Pair<>(account.getUsername(), identification.getValue()), account.getPassword())) {
                showInfoDialog(AlertType.INFORMATION, "Information", "Information", "Password of your account has been successfully changed.");
                account.setPassword(identification.getValue());
                account.setUsername(identification.getKey());
            } else {
                showInfoDialog(AlertType.ERROR, "Error", "Error", "Error executing SQL command. Password has not been changed.");
            }
        }
    }
}
