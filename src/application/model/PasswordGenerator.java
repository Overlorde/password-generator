package application.model;

import static java.lang.Math.random;

public class PasswordGenerator {


    /**
     * * Generate the password
     * * All -> 32 => 122
     * * Character -> 32 -> 48 => 57
     * * Specials 65 => 90
     * * Figure 97 => 122
     *
     * @param isWithSpecialCharacter the choice of character
     * @param sizePassword    the size of the password
     * @return the StringBuffer password
     */
    public static StringBuffer generatePassword(boolean isWithSpecialCharacter, int sizePassword) {
        StringBuffer passwordGenerated = new StringBuffer();
        int count = 0;
        int number;
        if (sizePassword > 0) {
            while (count != sizePassword) {
                if (isWithSpecialCharacter) { // with special characters
                    number = 33 + (int) (random() * (122 - 33)); // start at 33 to avoid [SPACE] character
                } else { // without special characters
                    do {
                        number = 47 + (int) (random() * (122 - 47)); // (max - min)
                    } while (number < 48 || (number > 57 && number < 65) || (number > 90 && number < 97));
                }
                passwordGenerated.append((char) number);
                count++;
            }
        }else {
            throw new IllegalArgumentException("Size of a password cannot be inferior to 1.");
        }
        return passwordGenerated;
    }
}