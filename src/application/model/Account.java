package application.model;

import javafx.util.Pair;

public class Account {
    private String username;
    private String password;

    /**
     * Constructor of account
     *
     * @param credentials the account's credentials
     */
    public Account(Pair<String, String> credentials) {
        this.username = credentials.getKey();
        this.password = credentials.getValue();
    }

    /**
     * Getter of password
     *
     * @return the account's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter of password
     *
     * @param password the account's password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter of username
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter of username
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}


