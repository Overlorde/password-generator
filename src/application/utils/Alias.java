package application.utils;

public enum Alias {
    DB("db-secret"),
    ADMIN("admin-secret"),
    ACCOUNT("app-secret");

    private final String alias;

    Alias(String alias){
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
