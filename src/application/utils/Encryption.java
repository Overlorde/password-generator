package application.utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Base64;

public class Encryption {

    public static final int KEY_SIZE = 256;
    private static final String KEY_STORE_DB = "keyStoreDB.jks";

    /**
     * Getter of KEY_STORE_DB
     *
     * @return KEY_STORE_DB
     */
    public static String getKeyStoreDb() {
        return KEY_STORE_DB;
    }

    /**
     * Create a secret Key
     *
     * @return a secret Key
     * @throws NoSuchAlgorithmException a NoSuchAlgorithm Exception
     */
    private static SecretKey getKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(KEY_SIZE);
        return keyGenerator.generateKey();
    }

    /**
     * Create a secret Key used with AES algorithm and store it
     *
     * @param password the hidden store password
     * @return a boolean status
     */
    public static KeyStore getOrCreateKeyStore(boolean create, String password) {
        KeyStore ks = null;
        FileInputStream fileInputStream = null;
        try {
            if (!create) {
                fileInputStream = new FileInputStream(KEY_STORE_DB);
            }
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(fileInputStream, password.toCharArray());
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return ks;
    }

    /**
     * Create a secret Key with AES algorithm and store it inside a KeyStore protected by password
     *
     * @param alias    the secret key linked to the alias
     * @param ks       the KeyStore
     * @param password the password to protect access to KeyStore
     * @return Boolean
     * @throws NoSuchAlgorithmException a NoSuchAlgorithm Exception
     * @throws KeyStoreException        a KeyStoreException Exception
     */
    public static Boolean generateEntryKeyStore(String alias, KeyStore ks, String password) throws NoSuchAlgorithmException, KeyStoreException {
        KeyStore.SecretKeyEntry secret = new KeyStore.SecretKeyEntry(getKey());
        KeyStore.ProtectionParameter secretPassword = new KeyStore.PasswordProtection(password.toCharArray()); // password should be encrypted to protected Keystore
        ks.setEntry(alias, secret, secretPassword); // store a new key which will be the bootpassword or admin password for instance in a keystore loaded and protected by user password
        try (FileOutputStream fos = new FileOutputStream(KEY_STORE_DB)) {
            ks.store(fos, password.toCharArray()); // save the new generated entry
            return true;
        } catch (CertificateException | NoSuchAlgorithmException | IOException | KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Update the entry of a KeyStore by its alias
     * @param ks the KeyStore
     * @param alias the alias
     * @param secret the private key
     * @param password the password which protect the KeyStore
     * @return Boolean
     * @throws KeyStoreException a KeyStoreException exception
     * @throws IOException an IOException exception
     * @throws CertificateException a CertificateException exception
     * @throws NoSuchAlgorithmException a NoSuchAlgorithmException exception
     */
    public static Boolean updateEntryKeyStore(KeyStore ks, String alias, byte[] secret, String password) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore.ProtectionParameter secretPassword = new KeyStore.PasswordProtection(password.toCharArray());
        try (FileOutputStream fos = new FileOutputStream(KEY_STORE_DB)) { // todo uninitialized keystore
            ks.setEntry(alias, new KeyStore.SecretKeyEntry(new SecretKeySpec(secret, "AES")), secretPassword);
            ks.store(fos, password.toCharArray()); // save the new generated entry
            return true;
        } catch (CertificateException | NoSuchAlgorithmException | IOException | KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Delete the entry of a KeyStore by its alias
     * @param ks the KeyStore
     * @param alias the alias
     * @return Boolean
     */
    public static Boolean deleteEntryKeyStore(KeyStore ks, String alias) {
        try {
            ks.deleteEntry(alias);
            return true;
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Encode a sentence with AES algorithm
     *
     * @param sentence     the sentence to encode
     * @param keyParameter the secret Key
     * @return the encoded sentence
     */
    protected static String encrypt(String sentence, byte[] keyParameter) {
        try {
            SecretKeySpec key = new SecretKeySpec(keyParameter, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return Base64.getEncoder().encodeToString(cipher.doFinal(sentence.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Encode a sentence with AES algorithm
     *
     * @param sentence     the sentence to decode
     * @param keyParameter the secret Key
     * @return the decoded sentence
     */
    protected static String decrypt(String sentence, byte[] keyParameter) {
        try {
            SecretKeySpec key = new SecretKeySpec(keyParameter, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(Base64.getDecoder().decode(sentence.getBytes(StandardCharsets.UTF_8))));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get the Secret Key from Java hidden store
     *
     * @param alias    the name of the store
     * @param password the password of the store
     * @return the Key
     */
    public static byte[] getKeyFromStore(String alias, String password) {
        try {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            try (InputStream keyStoreData = new FileInputStream(KEY_STORE_DB)) {
                ks.load(keyStoreData, password.toCharArray());
            } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
                return null;
            }
            return ks.getKey(alias, password.toCharArray()).getEncoded();
        } catch (KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException e) {
            return null;
        }
    }
}
