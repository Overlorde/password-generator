package application.utils;

public enum ModalType {
    LOGIN,
    REGISTER,
    UPDATE,
    SIGNUP,
    ACCOUNT
}
