package application.utils;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.UnexpectedException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static application.utils.Encryption.generateEntryKeyStore;
import static java.sql.DriverManager.println;

public class JDBCManager {
    public static final String DB_OWNER = "sysadm";
    private static final Logger logger = Logger.getLogger(JDBCManager.class.getName());
    static final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DB_NAME = "derby_storage";

    private static Connection connect;


    /**
     * Initializes the database with its components
     *
     * @param identification the identification of the user
     */
    public static void initializeDatabase(Pair<String, String> identification) {
        // this turns on NATIVE authentication as well as
        // SQL authorization
        System.setProperty("derby.authentication.provider", "NATIVE:" + "derby_storage" + ":LOCAL");
        deleteDatabaseDirectory();
        KeyStore ks;
        try {
            ks = createDatabaseKeyStore(identification.getValue());
            // create a private key to encrypt db password
            if (generateEntryKeyStore(Alias.DB.getAlias(), ks, identification.getValue()) && generateEntryKeyStore(Alias.ADMIN.getAlias(), ks, identification.getValue()) && generateEntryKeyStore(Alias.ACCOUNT.getAlias(), ks, identification.getValue())) {
                String connectionURL = getConnectionURL(Alias.ADMIN.getAlias(), new Pair<>(DB_OWNER, identification.getValue()), true, false);
                connect = DriverManager.getConnection(connectionURL); // return exception if already exist
                createUser(identification); // create user account by administrator
                createTable(identification.getKey());
                createRole();
                grantRole(identification.getKey());
                shutdownEngine(identification.getValue());
                connect.close();
            } else {
                throw new UnexpectedException("Unexpected error while creating new entry in KeyStore.");
            }
            if (tryToConnectWithoutCredentials()) { // Non SQL transient connection error
                throw new UnexpectedException("SQL error not expected without credentials to connect to database.");
            }
        } catch (SQLException | NoSuchAlgorithmException | UnexpectedException | KeyStoreException e) {
            logger.info(e.getMessage());
        }
    }

    /**
     * Deletes the database directory
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void deleteDatabaseDirectory() {
        try {
            Files.walk(Paths.get(DB_NAME))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Allows to connect to database
     *
     * @param identification the identification of the user
     * @return Boolean
     */
    public static Boolean connectDatabase(Pair<String, String> identification) {
        if (identification != null) {
            try {
                Class.forName(JDBC_DRIVER);
                String connectionURL = getConnectionURL(Alias.ACCOUNT.getAlias(), identification, false, false);
                connect = DriverManager.getConnection(connectionURL);
                connect.setSchema(identification.getKey());
                return true;
            } catch (Exception e) {
                logger.log(Level.WARNING, e.getMessage());
                return false;
            }
        }
        return true;
    }

    /**
     * Creates a user of the database
     *
     * @param identification the identification of the user
     */
    private static void createUser(Pair<String, String> identification) {
        try {
            PreparedStatement ps = connect.prepareStatement("call syscs_util.syscs_create_user( ?, ? )");
            ps.setString(1, String.format("\"%s\"", identification.getKey()));
            ps.setString(2, Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), identification.getValue())));
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Creates a role with associated privileges
     */
    private static void createRole() {
        try {
            PreparedStatement ps = connect.prepareStatement(String.format("CREATE ROLE %s", "app_user"));
            ps.execute();
            ps.close();
            ps = connect.prepareStatement(String.format("GRANT SELECT, INSERT, DELETE, UPDATE ON ApplicationStore TO %s", "app_user"));
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Grants a role to a specific user
     *
     * @param user the username of the user
     */
    private static void grantRole(String user) {
        try {
            PreparedStatement ps = connect.prepareStatement(String.format("GRANT app_user TO \"%s\"", user));
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Creates a table
     */
    private static void createTable(String user) {
        try {
            PreparedStatement preparedStatementSchema = connect.prepareStatement(String.format("CREATE SCHEMA \"%s\" AUTHORIZATION \"%s\"", user, user));
            preparedStatementSchema.execute();
            preparedStatementSchema.close();
            connect.setSchema(user);
            PreparedStatement ps = connect.prepareStatement("CREATE TABLE ApplicationStore(" +
                    "applicationName VARCHAR(30) NOT NULL," +
                    "applicationPassword VARCHAR(255) NOT NULL," +
                    "PRIMARY KEY (applicationName))");
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Try to connect to database without supplying credentials
     *
     * @return Boolean
     */
    private static Boolean tryToConnectWithoutCredentials() {
        println("Trying to connect without supplying credentials...");
        try {
            String connectionURL = getConnectionURL(Alias.ACCOUNT.getAlias(), null, false, false);
            DriverManager.getConnection(connectionURL);
            println("ERROR: Unexpectedly connected to database " + DB_NAME);
            return true;
        } catch (SQLException e) {
            logger.log(Level.INFO, e.getMessage());
        }
        return false;
    }

    private static void shutdownEngine(String password) {
        String shutdownURL = getConnectionURL(Alias.ADMIN.getAlias(), new Pair<>(DB_OWNER, password), false, true);
        try {
            DriverManager.getConnection(shutdownURL);
        } catch (SQLException se) {
            if (se.getSQLState().equals("XJ015")) {
                logger.log(Level.SEVERE, "Derby engine shutdown normally.");
            }
        }
    }


    /**
     * Connects to database with granted rights corresponding to input user
     *
     * @param alias          the input alias corresponding to a private key in KeyStore
     * @param identification the input pair username & password
     * @param createDB       a boolean to indicate if database must be created
     * @param shutdownDB     a boolean to indicate if database must be shutdown
     * @return String the URL connection to database
     */
    private static String getConnectionURL(String alias, Pair<String, String> identification, boolean createDB, boolean shutdownDB) {
        String connectionURL = "jdbc:derby:".concat(DB_NAME);
        connectionURL = connectionURL.concat(String.format(";shutdown=%s", shutdownDB));
        if (identification != null) {
            connectionURL = connectionURL.concat(String.format(";user=\"%s\"", identification.getKey())).concat(";password=" + Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(alias, identification.getValue())));
            if (createDB) {
                connectionURL = connectionURL.concat(";create=true").concat(";dataEncryption=true"); // only require at creation
            }
            connectionURL = connectionURL.concat(String.format(";bootPassword=%s", Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(Alias.DB.getAlias(), identification.getValue()))));
        }
        return connectionURL;
    }


    /**
     * Load the Secret Key from KeyStore
     *
     * @param password the password to decrypt the KeyStore
     * @return the list of application and password stored
     * @throws SQLException an SQL Exception
     */
    public static ArrayList<Pair<String, Integer>> loadApplicationsFromStorage(String password) throws SQLException {
        ArrayList<Pair<String, Integer>> applicationPassword = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connect.prepareStatement("SELECT * FROM ApplicationStore");
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) { //check if exist a row
                applicationPassword.add(new Pair<>(Encryption.decrypt(rs.getString("applicationName"),
                        Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)), (Objects.requireNonNull(Encryption.decrypt(rs.getString("applicationPassword"),
                        Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)))).length())
                );
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new SQLException("An error occurred in function loadApplicationsFromStorage : ", e);
        }
        return applicationPassword;
    }

    /**
     * Load the password of a specific application from KeyStore
     *
     * @param application the application name
     * @param password    the password to decrypt the KeyStore
     * @return the password of the specified application
     * @throws SQLException an SQL Exception
     */
    public static String loadPasswordsFromStorage(String application, String password) throws SQLException {
        String applicationPassword = null;
        try {
            PreparedStatement preparedStatement = connect.prepareStatement("SELECT applicationPassword FROM ApplicationStore WHERE applicationName=?");
            preparedStatement.setString(1, Encryption.encrypt(application, Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)));
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) { //check if exist a row
                applicationPassword = Encryption.decrypt(rs.getString("applicationPassword"), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            throw new SQLException("An error occurred in function loadPasswordsFromStorage : ", e);
        }
        return applicationPassword;
    }


    /**
     * Set a new password to encrypt the database
     *
     * @param password the user's password
     */
    private static KeyStore createDatabaseKeyStore(String password) { //not good format method
        return Encryption.getOrCreateKeyStore(true, password);
    }

    /**
     * Delete the application from the KeyStore
     *
     * @param applicationName the application name
     * @param password        the password of the account
     * @return the success of delete operation
     */
    public static Boolean deleteApplicationFromKeyStore(String applicationName, String password) {
        try {
            connect.beginRequest();
            PreparedStatement preparedStatement = connect.prepareStatement("""
                    DELETE FROM ApplicationStore
                    WHERE
                    applicationName=?""");
            preparedStatement.setString(1, Encryption.encrypt(applicationName, Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)));
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connect.commit();
            return true;
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
            return false;
        }
    }

    /**
     * Update the password of a specific application
     *
     * @param application the application name and its password
     * @param password    the new password of the application
     * @return the success of the update
     */
    public static Boolean updateApplicationFromKeyStore(Pair<String, String> application, String password) {
        try {
            connect.beginRequest();
            PreparedStatement preparedStatement = connect.prepareStatement("""
                    UPDATE ApplicationStore
                    SET applicationPassword=?
                    WHERE applicationName=?""");
            preparedStatement.setString(1, Encryption.encrypt(application.getValue(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)));
            preparedStatement.setString(2, Encryption.encrypt(application.getKey(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)));
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connect.commit();
            return true;
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
            return false;
        }
    }

    /**
     * Insert a new application into the KeyStore
     *
     * @param application the application name and password
     * @param password    the user's password
     * @return the success of the insertion
     */
    public static Boolean insertApplicationRecord(Pair<String, String> application, String password) {
        try {
            connect.beginRequest();
            PreparedStatement preparedStatement = connect.prepareStatement("INSERT INTO ApplicationStore(" +
                    "applicationName," +
                    "applicationPassword" +
                    ") VALUES (" + "'" +
                    Encryption.encrypt(application.getKey(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)) + "'"
                    + ","
                    + "'" + Encryption.encrypt(application.getValue(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), password)) + "'" + ")");
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connect.commit();
            return true;
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
            return false;
        }
    }

    /**
     * Change the account's password
     *
     * @param identification the new credentials
     * @param oldPassword    the old password
     * @return the success of updating the password
     */
    public static Boolean updateAccountPassword(Pair<String, String> identification, String oldPassword) {
        try {
            // todo newBootPassword not working
            String connectionURL = getConnectionURL(Alias.ADMIN.getAlias(), new Pair<>(DB_OWNER, oldPassword), false, false)
                    .concat(";newBootPassword=" + Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(Alias.DB.getAlias(), oldPassword)));
            // update bootPassword at the same time as the private key will remain the same
            DriverManager.getConnection(connectionURL);
            shutdownEngine(oldPassword);
            Connection connection = DriverManager.getConnection(getConnectionURL(Alias.ADMIN.getAlias(), new Pair<>(DB_OWNER, oldPassword), false, false));
            // change password for each alias in keystore -> private key
            KeyStore ks = Encryption.getOrCreateKeyStore(false, oldPassword);
            byte[][] secrets = {Encryption.getKeyFromStore(Alias.DB.getAlias(), oldPassword), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), oldPassword), Encryption.getKeyFromStore(Alias.ADMIN.getAlias(), oldPassword)};

            Encryption.deleteEntryKeyStore(ks, Alias.DB.getAlias());
            Encryption.deleteEntryKeyStore(ks, Alias.ACCOUNT.getAlias());
            Encryption.deleteEntryKeyStore(ks, Alias.ADMIN.getAlias());

            Encryption.updateEntryKeyStore(ks, Alias.DB.getAlias(), secrets[0], identification.getValue());
            Encryption.updateEntryKeyStore(ks, Alias.ACCOUNT.getAlias(), secrets[1], identification.getValue());
            Encryption.updateEntryKeyStore(ks, Alias.ADMIN.getAlias(), secrets[2], identification.getValue());
            // update user password
            connection.beginRequest();
            PreparedStatement ps = connection.prepareStatement("call syscs_util.syscs_reset_password( ?, ? )");
            ps.setString(1, String.format("\"%s\"", identification.getKey()));
            ps.setString(2, Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(Alias.ACCOUNT.getAlias(), identification.getValue())));
            ps.execute();
            // update sysadmin password
            ps = connection.prepareStatement("call syscs_util.syscs_reset_password( ?, ? )");
            ps.setString(1, String.format("\"%s\"", DB_OWNER));
            ps.setString(2, Encryption.encrypt(identification.getValue(), Encryption.getKeyFromStore(Alias.ADMIN.getAlias(), identification.getValue())));
            ps.execute();
            connection.commit();
            connection.close();
            return true;
        } catch (SQLException | KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException e) {
            logger.log(Level.WARNING, e.getMessage());
            return false;
        }
    }

    /**
     * Allows to check whether the database is created or not
     *
     * @return Boolean
     */
    public static Boolean isDatabaseCreated() {
        try {
            Class.forName(JDBC_DRIVER);
            DriverManager.getConnection(getConnectionURL(Alias.ACCOUNT.getAlias(), null, false, false));
            shutdownEngine(null);
            throw new SQLException("ERROR: Unexpectedly connected to database " + DB_NAME);
        } catch (SQLException e) {
            // database not found
            if (e.getSQLState().equals("XJ004")) {
                return false;
            }
            // database requires identification
            else if (e.getSQLState().equals("XJ040")) {
                return true;
            }
        } catch (ClassNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return true;
    }
}
