package application;

import application.view.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;

import java.io.IOException;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    /**
     * Start the application stage
     *
     * @param primaryStage the stage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("PassGuard");
        this.primaryStage.getIcons().add(new Image("file:password.png"));

        initRootLayout();
        showPersonOverview();
    }

    /**
     * Initializes the root layout
     */
    private void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            JMetro jMetro = new JMetro(Style.LIGHT);
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = loader.load();
            // Show the scene containing the root layout.
            rootLayout.setMinSize(900, 600);
            primaryStage.setMinHeight(640); // 600 + 40 (margin resize)
            primaryStage.setMinWidth(924); // 900 + 24 (margin resize)
            Scene scene = new Scene(rootLayout);
            jMetro.setScene(scene);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout
     */
    private void showPersonOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(MainApp.class.getResource("view/MainApplication.fxml"));
            AnchorPane personOverview = loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            Controller controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the stage
     *
     * @return the stage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Launch the application
     *
     * @param args the args
     */
    public static void main(String[] args) {
        launch(args);
    }
}