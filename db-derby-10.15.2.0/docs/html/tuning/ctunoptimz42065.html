<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-us" xml:lang="en-us" xmlns="http://www.w3.org/1999/xhtml">
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="(C) Copyright 2005" name="copyright" />
<meta content="(C) Copyright 2005" name="DC.rights.owner" />
<meta content="public" name="security" />
<meta content="index,follow" name="Robots" />
<meta content='(PICS-1.1 "http://www.icra.org/ratingsv02.html" l gen true r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true r (n 0 s 0 v 0 l 0) "http://www.classify.org/safesurf/" l gen true r (SS~~000 1))' http-equiv="PICS-Label" />
<meta content="concept" name="DC.Type" />
<meta content="Transaction-based lock escalation" name="DC.Title" />
<meta content="The optimizer makes its decisions for the scope of a single statement at compile time; the runtime overrides are also for the scope of a single statement." name="abstract" />
<meta content="The optimizer makes its decisions for the scope of a single statement at compile time; the runtime overrides are also for the scope of a single statement." name="description" />
<meta content="Lock escalation, at runtime (for transaction)" name="DC.subject" />
<meta content="Lock escalation, at runtime (for transaction)" name="keywords" />
<meta content="ctunoptimz27975.html" name="DC.Relation" scheme="URI" />
<meta content="ctunoptimz11775.html" name="DC.Relation" scheme="URI" />
<meta content="XHTML" name="DC.Format" />
<meta content="ctunoptimz42065" name="DC.Identifier" />
<meta content="en-us" name="DC.Language" />
<link href="commonltr.css" rel="stylesheet" type="text/css" />
<title>Transaction-based lock escalation</title>
</head>
<body id="ctunoptimz42065"><a name="ctunoptimz42065"><!-- --></a>


<h1 class="topictitle1">Transaction-based lock escalation</h1>



<div><p>The optimizer makes its decisions for the scope of a single statement
at compile time; the runtime overrides are also for the scope of a single
statement.</p>

<p>As you know, a transaction can span several statements. For connections
running in TRANSACTION_SERIALIZABLE isolation and for connections that are doing
a lot of inserts or updates, a transaction can accumulate a number of row locks
even though no single statement would touch enough rows to make the optimizer
choose table-level locking for any single table.</p>

<p>However, during a transaction, the
<span>Derby</span> system tracks the
number of locks for all tables in the transaction, and when this number exceeds
a threshold number (which you can configure; see
<a href="ctunoptimz26019.html">Lock escalation threshold</a>), the system attempts to escalate locking
for at least one of the tables involved from row-level to table-level
locking.</p>

<p>The system attempts to escalate to table-level locking for each table that
has a burdensome number of locks by trying to obtain the relevant table lock. If
the system can lock the table without waiting, the system locks the entire table
and releases all row locks for the table. If the system cannot lock the table
without waiting, the system leaves the row locks intact.</p>

<p>After a table is locked in either mode, a transaction does not acquire any
subsequent row-level locks on a table. For example, if you have a table called
<samp class="codeph">Hotels</samp> that contains several thousand rows and a transaction
locks the entire table in share mode in order to read data, it might later need
to lock a particular row in exclusive mode in order to update the row. However,
the previous table-level lock on <samp class="codeph">Hotels</samp> forces the exclusive
lock to be table-level as well.</p>

<p>This transaction-based runtime decision is independent of any compilation
decision.</p>

<p>If when the escalation threshold was exceeded the system did not obtain any
table locks because it would have had to wait, the next lock escalation attempt
is delayed until the number of held locks has increased by some significant
amount, for example from 5000 to 6000.</p>

<p>Here are some examples, assuming the escalation threshold is 5000.</p>
 
<p>In the following table, a single database table holds the majority of the
locks.</p>


<div class="tablenoborder"><table border="1" cellpadding="4" cellspacing="0" frame="border" rules="all" summary="This table shows an example of lock escalation when one table holds the majority of the locks."><caption>Table 1. Single table holding the majority of the locks</caption>



<thead align="left">
<tr valign="bottom">
<th id="N100C1" valign="bottom" width="28.999999999999996%">Table</th>

<th id="N100C8" valign="bottom" width="47%">Number of Row Locks</th>

<th id="N100CF" valign="bottom" width="24%">Escalate?</th>
</tr>

</thead>

<tbody>
<tr>
<td headers="N100C1" valign="top" width="28.999999999999996%"><samp class="codeph">Hotels</samp></td>

<td headers="N100C8" valign="top" width="47%">4853</td>

<td headers="N100CF" valign="top" width="24%">Yes</td>
</tr>

<tr>
<td headers="N100C1" valign="top" width="28.999999999999996%"><samp class="codeph">Countries</samp></td>

<td headers="N100C8" valign="top" width="47%">3</td>

<td headers="N100CF" valign="top" width="24%">No</td>
</tr>

<tr>
<td headers="N100C1" valign="top" width="28.999999999999996%"><samp class="codeph">Cities</samp></td>

<td headers="N100C8" valign="top" width="47%">12</td>

<td headers="N100CF" valign="top" width="24%">No</td>
</tr>

</tbody>

</table>
</div>

<p>In the following table, two database tables hold the majority of the
locks.</p>


<div class="tablenoborder"><table border="1" cellpadding="4" cellspacing="0" frame="border" rules="all" summary="This table shows an example of lock escalation when two tables hold the majority of the locks."><caption>Table 2. Two tables holding the majority of the locks</caption>



<thead align="left">
<tr valign="bottom">
<th id="N1017C" valign="bottom" width="28.999999999999996%">Table</th>

<th id="N10183" valign="bottom" width="47%">Number of Row Locks</th>

<th id="N1018A" valign="bottom" width="24%">Escalate?</th>
</tr>

</thead>

<tbody>
<tr>
<td headers="N1017C" valign="top" width="28.999999999999996%"><samp class="codeph">Hotels</samp></td>

<td headers="N10183" valign="top" width="47%">2349</td>

<td headers="N1018A" valign="top" width="24%">Yes</td>
</tr>

<tr>
<td headers="N1017C" valign="top" width="28.999999999999996%"><samp class="codeph">Countries</samp></td>

<td headers="N10183" valign="top" width="47%">3 </td>

<td headers="N1018A" valign="top" width="24%">No</td>
</tr>

<tr>
<td headers="N1017C" valign="top" width="28.999999999999996%"><samp class="codeph">Cities</samp></td>

<td headers="N10183" valign="top" width="47%">1800</td>

<td headers="N1018A" valign="top" width="24%">Yes</td>
</tr>

</tbody>

</table>
</div>

<p>In the following table, many database tables hold a small number of
locks.</p>


<div class="tablenoborder"><table border="1" cellpadding="4" cellspacing="0" frame="border" rules="all" summary="This table shows an example of lock escalation when many tables hold a small number of locks."><caption>Table 3. Many tables holding a small number of locks</caption>



<thead align="left">
<tr valign="bottom">
<th id="N10237" valign="bottom" width="27%">Table</th>

<th id="N1023E" valign="bottom" width="48%">Number of Row Locks</th>

<th id="N10245" valign="bottom" width="25%">Escalate?</th>
</tr>

</thead>

<tbody>
<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table001</samp></td>

<td headers="N1023E" valign="top" width="48%">279</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table002</samp></td>

<td headers="N1023E" valign="top" width="48%">142</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table003</samp></td>

<td headers="N1023E" valign="top" width="48%">356</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table004</samp></td>

<td headers="N1023E" valign="top" width="48%">79</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table194</samp></td>

<td headers="N1023E" valign="top" width="48%">384</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

<tr>
<td headers="N10237" valign="top" width="27%"><samp class="codeph">table195</samp></td>

<td headers="N1023E" valign="top" width="48%">416</td>

<td headers="N10245" valign="top" width="25%">No</td>
</tr>

</tbody>

</table>
</div>

</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="ctunoptimz27975.html" title="Row-level locking improves concurrency in a multi-user system. However, a large number of row locks can degrade performance.">Locking and performance</a></div>
</div>
<div class="relconcepts"><strong>Related concepts</strong><br />
<div><a href="ctunoptimz11775.html" title="You can explicitly lock a table for the duration of a transaction with the LOCK TABLE statement.">Locking a table for the duration of a transaction</a></div>
</div>
</div>

</body>
</html>
